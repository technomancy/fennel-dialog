(set chars.stan {:x 56 :y 172 :name "stan"
                 :spr 288 :portrait 289})

(set chars.bob {:x 32 :y 52 :name "bob"
                :spr 320 :portrait 321})

;; not actually a character, but you can "talk" to it to make things happen
(set chars.box {:x (* 33 8) :y (* 15 8) :name "box"})

(local all {})

(fn all.bob1 []
  (say "Hi! My name's Bob. I need help.")
  (if (= "Sure!" (ask "Can you get my key from Stan?"
                      ["Sure!" "No, I'm busy"]))
      (do (say "Great! He's to the south.")
          (set convos.stan all.stan2)
          (set convos.bob all.bob2))
      (say "Too bad.")))

(fn all.stan1 []
  (say "What's up, Captain Proton?")
  (say "I heard there was going to be trouble.")
  (if (= "Yes" (ask "Are you afraid?"
                    ["Yes" "No"]))
      (say "Yeah, me too!")
      (say "Wow, you're brave.")))

(fn all.stan2 []
  (say "So Bob thinks I have his key, does he?")
  (say "Well... it might be in the cargo room.")
  (say "Maybe you'll find it there.")
  (say "I would go get it myself, but"
       "I can't leave my station here.")
  (set convos.box all.box))

(fn all.bob2 []
  (say "Stan is to the south."))

(fn all.bob-key []
  (say "Oh, thank goodness! I was so worried.")
  (say "This means I'll be able to fly my ship.")
  (say "You really saved the day.")
  (set convos.stan all.stan1))

(fn all.stan-key []
  (say "Oh good, you found it!")
  (say "Better take it to Bob though."))

(fn all.box []
  (say "You find Bob's key.")
  (say "It was laying on one of the boxes.")
  (set convos.box nil)
  (set convos.stan all.stan-key)
  (set convos.bob all.bob-key))

(set convos.bob all.bob1)
(set convos.stan all.stan1)
