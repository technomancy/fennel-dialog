;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; collision

(var x 96)
(var y 24)

(local passable {0 true 1 true 17 true})

(fn open-bg? [x y]
  (. passable (mget (// x 8) (// y 8))))

(fn in-box? [c x y w h]
  (and (<= x c.x (+ x w))
       (<= y c.y (+ y h))))

(fn hit-ch? [x y]
  (fn [c]
    (and (<= c.x x (+ c.x 7))
         (<= c.y y (+ c.y 15)))))

(fn any? [f tbl]
  (var found false)
  (each [_ x (pairs tbl)]
    (when (or found (f x))
      (set found true)))
  found)

(fn can-move-pt? [x y]
  (and (open-bg? x y)
       (not (any? (hit-ch? x y) chars))))

(fn can-move? [x y]
  (and (can-move-pt? x y)
       (can-move-pt? (+ 5 x) y)
       (can-move-pt? x (+ 6 y))
       (can-move-pt? (+ 5 x) (+ 6 y))))

(fn move []
  (var dx 0)
  (var dy 0)
  (when (btn 0) (set dy -1))
  (when (btn 1) (set dy  1))
  (when (btn 2) (set dx -1))
  (when (btn 3) (set dx  1))
  (if (can-move? (+ x dx) (+ y dy 8))
      (set (x y) (values (+ x dx) (+ y dy)))
      (and (~= dx 0) (can-move? (+ x dx) (+ y 8)))
      (set x (+ x dx))
      (and (~= dy 0) (can-move? x (+ y dy 8)))
      (set y (+ y dy))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; draw etc

(var camera-x x)
(var camera-y y)

(local stars [])
(for [i 1 24]
  (table.insert stars {:x (math.random 2400)
                       :y (math.random 1360)}))

(fn draw-stars []
  (each [_ s (pairs stars)]
    (pix (% (* (+ s.x camera-x) 0.1) 240)
         (% (* (+ s.y camera-y) 0.1) 136) 1)
    (pix (% (* (+ s.x camera-x) 0.7) 240)
         (% (* (+ s.y camera-y) 0.7) 136) 15)))

(fn draw-dialog []
  (when said
    (rect 0 0 238 44 1)
    (rectb 1 1 236 42 15)
    (print said 24 6)
    (when (and who who.portrait)
      (print who.name 5 30)
      (spr who.portrait 5 10 0 1 0 0 2 2))
    (when choices
      (each [i ch (ipairs choices)]
        (when (= i choice)
          (print ">" 30 (+ 10 (* 8 i))))
        (print ch 38 (+ 10 (* 8 i)))))))

(fn lerp [a b t]
  (+ (* a (- 1 t)) (* t b)))

(fn draw []
  (cls)
  (set camera-x (math.min 120 (lerp camera-x (- 120 x) 0.05)))
  (set camera-y (math.min 64 (lerp camera-y (- 64 y) 0.05)))
  (draw-stars)
  (map (// (- camera-x) 8) (// (- camera-y) 8)
       32 19 (- (% camera-x 8) 8)
       (- (% camera-y 8) 8) 0)
  (each [name c (pairs chars)]
    (when c.spr
      (spr c.spr (+ camera-x c.x) (+ camera-y c.y) 0 1 0 0 1 2)))
  (spr 257 (+ x camera-x) (+ y camera-y) 0 1 0 0 1 2)
  (draw-dialog))

;;;;;;;;;;;;;;;;;;;; tie it all together

(fn tic []
  (draw)
  (let [talking-to (dialog x y (btnp 4) (btnp 5))]
    (if (and talking-to (btnp 0)) (choose -1)
        (and talking-to (btnp 1)) (choose 1)
        (not talking-to) (move)))
  (for [i (# coros) 1 -1]
    (coroutine.resume (. coros i))
    (when (= :dead (coroutine.status (. coros i)))
      (table.remove coros i))))

(global TIC tic)
