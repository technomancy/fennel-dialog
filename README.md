# fennel-dialog

This is a coroutine-based dialog system written in [Fennel][fnl], suitable
for use in [TIC-80][tic], [LÖVE][love], or another Lua-based game
system.

It includes a demo for TIC-80. If you have that installed, just run `make`,
or you can [play it in the browser](https://p.hagelb.org/dialog.tic.html).

In the demo, `dialog.fnl` contains the dialog engine, `characters.fnl`
contains the game data, and `game.fnl` contains the TIC-specific
bits.

There are also tests in `test.fnl`.

Copyright © 2019 Phil Hagelberg and contributors

Distributed under the GNU General Public License version 3 or later; see file LICENSE.

[fnl]: https://fennel-lang.org
[tic]: https://tic.computer
[love]: https://love2d.org
