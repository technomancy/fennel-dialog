(local fennel (require :fennel))
(local view (require :fennelview))
(fn p [x] (print (view x)))

(local dialog (require :dialog))

;; load in character definitions
(fennel.dofile "characters.fnl" {:env dialog})

(fn coords-for [chars name]
  (let [char (. chars name)]
    (if char (values char.x char.y))))

(fn assert-dialog [x y name said]
  (let [state (dialog.dialog x y true)]
    (assert state (.. name " not found."))
    (assert (= name state.who.name) (.. "Expected conversation with " name))
    (assert (: state.said :find said) (.. "Expected to say " said))
    state))

(fn assert-end [x y]
  (assert (not (dialog.dialog x y true))))

(fn assert-convo [name ...]
  (let [(x y) (coords-for dialog.chars name)]
    (each [_ u (ipairs [...])]
      (match u
        [2 reply]
        (do (dialog.choose 1) ; down to second option
            (assert-dialog x y name reply))
        [question choice1 choice2]
        (let [state (assert-dialog x y name question)]
          (assert (= choice1 (. state :choices 1)))
          (assert (= choice2 (. state :choices 2))))
        utterance
        (assert-dialog x y name utterance)))
    (assert-end x y)))

(assert-convo :stan
              "What's up"
              "I heard"
              ["afraid?" "Yes" "No"]
              "me too")

(assert-convo :stan
              "What's up"
              "I heard"
              ["afraid?" "Yes" "No"]
              [2 "you're brave"])

;; Turn down quest
(assert-convo :bob
              "Hi!"
              "key from Stan?"
              [2 "Too bad"])

;; Accept quest
(assert-convo :bob
              "Hi!"
              "key from Stan?"
              "Great!")

(assert-convo :stan
              "Bob thinks I have his key"
              "cargo room"
              "Maybe you'll find it")

;; find item
(assert-convo :box
              "find Bob's key"
              "laying on one of the boxes")

;; complete quest
(assert-convo :bob
              "thank goodness"
              "I'll be able to fly"
              "saved the day")

(print "Passed!")
