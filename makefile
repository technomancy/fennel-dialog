TIC_GLOBALS=mget,btn,btnp,map,spr,rect,rectb,trace,pix,mset,cls

run: out.fnl; tic80 dialog.tic -code-watch out.fnl

out.fnl: header.fnl dialog.fnl characters.fnl game.fnl
	cat $^ > $@

check: out.fnl
	fennel --globals $(TIC_GLOBALS) --compile $< > /dev/null

test: check; fennel test.fnl
